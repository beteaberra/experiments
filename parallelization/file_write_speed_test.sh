# time in nanoseconds since 1970 in both filename and content (3 numbers, 3 files)
echo $(date +%s%N)-new > $(date +%s%N).new.log ; echo $(date +%s%N)-new > $(date +%s%N).new.log ; echo $(date +%s%N)-new > $(date +%s%N).new.log ;

sleep 1

# time in nanoseconds since 1970 in both filename and appended to content (3 numbers, 3 files)
echo $(date +%s%N)-append >> $(date +%s%N).append.log ; echo $(date +%s%N)-append >> $(date +%s%N).append.log ;  echo $(date +%s%N)-append >> $(date +%s%N).append.log ;

sleep 1

echo $(date +%s%N)-1-blocking > speed-nonblocking.log 
echo $(date +%s%N)-2-nonblocking >> speed-nonblocking.log & 
echo $(date +%s%N)-3-nonblocking >> speed-nonblocking.log & 
echo $(date +%s%N)-4-nonblocking >> speed-nonblocking.log & 
echo $(date +%s%N)-5-nonblocking >> speed-nonblocking.log & 
echo $(date +%s%N)-6-nonblocking >> speed-nonblocking.log & 
echo $(date +%s%N)-7-nonblocking >> speed-nonblocking.log &

sleep 1

echo $(date +%s%N)-1-blocking > speed-blocking.log 
echo $(date +%s%N)-2-nonblocking >> speed-blocking.log
echo $(date +%s%N)-3-nonblocking >> speed-blocking.log
echo $(date +%s%N)-4-nonblocking >> speed-blocking.log
echo $(date +%s%N)-5-nonblocking >> speed-blocking.log
echo $(date +%s%N)-6-nonblocking >> speed-blocking.log
echo $(date +%s%N)-7-nonblocking >> speed-blocking.log

sleep 1

tail -n +1 *.log > file_write_speed_test.files.txt

rm -f *.log



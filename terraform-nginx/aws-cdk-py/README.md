## References

- CDK python docs [nginx](https://learn.hashicorp.com/tutorials/terraform/cdktf-build-python)
- CDK for Terraform [getting started with python & typescript](https://learn.hashicorp.com/tutorials/terraform/cdktf-build-python)

## Get Started

```bash
npm install -g cdktf-cli
conda activate base
conda install pipenv
conda install -c conda-forge pipenv
conda activate nlpia2
conda install pipenv
conda install -c conda-forge pipenv
cdktf init
```



# www-1
resource "digitalocean_droplet" "nginx-1" {
  image = "ubuntu-20-04-x64"
  name = "nginx-1"
  region = "nyc3"
  size = "s-1vcpu-1gb"
  ssh_keys = [
    data.digitalocean_ssh_key.terraform.id
  ]

  connection {
    host = self.ipv4_address
    user = "root"
    type = "ssh"
    private_key = file(var.pvt_key)
    timeout = "2m"
  }

  provisioner "remote-exec" {
    inline = [
      "export PATH=$PATH:/usr/bin",
      # install nginx
      "sudo apt update",
      "sudo apt install -y nginx"
    ]
  }

}


# data "digitalocean_ssh_key" "ssh_key" {
#   # -var "pvt_key=$HOME/.ssh/id_rsa_do_tanbot"
#   name = "terraform_tanbot_ssh_key"
# }

# resource "digitalocean_droplet" "nginx-1" {
#     image = "ubuntu-20-04-x64"
#     name = "nginx-1"
#     region = "nyc3"
#     size = "s-1vcpu-1gb"
#     backups            = false
#     ipv6               = true

#     # see DO API docs to obtain key signatures/ids:
#     # https://developers.digitalocean.com/documentation/v2/#list-all-keys
#     ssh_keys = [
#       data.digitalocean_ssh_key.terraform.id
#     ]

#     connection {
#         host        = self.ipv4_address
#         user        = "root"
#         type        = "ssh"
#         private_key = file(var.pvt_key)
#         timeout = "2m"
#     }

#     # from: https://github.com/digitalocean/terraform-provider-digitalocean/blob/main/examples/droplet/main.tf
#     # connection {
#     #   private_key = file("~/.ssh/id_rsa")
#     #   ... }

#     provisioner "remote-exec" {
#         inline = [
#             "export PATH=$PATH:/usr/bin",
#             # install nginx
#             "sudo apt update",
#             "sudo apt install -y nginx"
#         ]
#     }
# }

# Slides

## Algorithm

### Repeat Indefinitely:
    
    ```python
    while True:
    ```

#### 1. Train **C**lassifier on **T**rainingData

        ```python
        C.fit(T)
        ```

#### 2. Evaluate **C**lassifier on **V**alidationData and decide if done

        ```python
        if C.score(V) > required_accuracy:
            break
        ```

#### 3. Estimate labels for **U**nlabeledData with **C**lassifier

        ```python 
        C.predict(U)
        ```

#### 4. Select **D**ifficultData examples based on uncertainty and density

        ```python
        D = U.sort_values('uncertainty*density')[-10:]  
        ```

#### 5. Split **D**ifficultData into **D**ifficult**T**rainingData and **D**ifficult**V**alidationData

        ```python
        DT, DV = split(H.predict(D))
        ```

#### 6. Augment **T**rainingData with newly labeled **D**ifficult**T**rainingData

        ```python
        T.extend(DT)
        V.extend(DV)
        ```

# References - Active Learning

#### WSD Data (word sense disambiguation)

- Training: http://lcl.uniroma1.it/wsdeval/data/WSD_Training_Corpora.zip
- Evaluation: http://lcl.uniroma1.it/wsdeval/data/WSD_Unified_Evaluation_Datasets.zip

### active learning meta meta analysis

#### https://www.pnas.org/content/117/12/6476

Active learning narrows achievement gaps for underrepresented students in undergraduate science, technology, engineering, and math

Elli J. Theobald, Mariah J. Hill, Elisa Tran, Sweta Agrawal, E. Nicole Arroyo, Shawn Behling, Nyasha Chambwe, Dianne Laboy Cintrón, Jacob D. Cooper, Gideon Dunster, Jared A. Grummer, Kelly Hennessey, Jennifer Hsiao, Nicole Iranon, Leonard Jones II, [View ORCID Profile](http://orcid.org/0000-0002-0709-0294)Hannah Jordt, Marlowe Keller, Melissa E. Lacey, Caitlin E. Littlefield, Alexander Lowe, Shannon Newman, Vera Okolo, Savannah Olroyd, Brandon R. Peecook, Sarah B. Pickett, David L. Slager, Itzue W. Caviedes-Solis, Kathryn E. Stanchak, Vasudha Sundaravardan, Camila Valdebenito, Claire R. Williams, Kaitlin Zinsli, and [View ORCID Profile](http://orcid.org/0000-0003-0988-1900)Scott Freeman
PNAS March 24, 2020 117 (12) 6476-6483; first published March 9, 2020; [https://doi.org/10.1073/pnas.1916903117](https://doi.org/10.1073/pnas.1916903117)

Data: 
https://github.com/ejtheobald/Gaps_Metaanalysis/blob/master/DataFinal/PercPass_Final.csv


## machine active learning

#### Active learning with sampling by uncertainty and density for word sense disambiguation and text classification

Jingbo Zhu, Huizhen Wang, Tianshun Yao, Benjamin K Tsou

Proceedings of the 22nd International Conference on Computational Linguistics (Coling 2008), 1137-1144, 2008

This paper addresses two issues of active learning. Firstly, to solve a problem of uncertainty sampling that it often fails by selecting outliers, this paper presents a new selective sampling technique, sampling by uncertainty and density (SUD), in which a k-Nearest-Neighbor-based density measure is adopted to determine whether an unlabeled example is an outlier. Secondly, a technique of sampling by clustering (SBC) is applied to build a representative initial training data set for active learning. Finally, we implement a new algorithm of active learning with SUD and SBC techniques. The experimental results from three real-world data sets show that our method outperforms competing methods, particularly at the early stages of active learning.

Input:
  L: training set (1 labeled example?)
  T: test set (1 labeled example?)
  U: unlabeled examples (limitless?)
  H: human labeler

### Repeat: 

#### 1. Train **C**lassifier on **T**rainingData

        ```python
        C.fit(T)
        ```

#### 2. Evaluate **C**lassifier on **V**alidationData and decide if done

        ```python
        if C.score(V) > required_accuracy:
            break
        ```

#### 3. Estimate labels for **U**nlabeledData with **C**lassifier

        ```python 
        C.predict(U)
        ```

#### 4. Select **D**ifficultData examples based on uncertainty and density

        ```python
        D = U.sort_values('uncertainty*density')[-10:]  
        ```

#### 5. Split **D**ifficultData into **D**ifficult**T**rainingData and **D**ifficult**V**alidationData

        ```python
        DT, DV = split(H.predict(D))
        ```

#### 6. Augment **T**rainingData with newly labeled **D**ifficult**T**rainingData

        ```python
        T.extend(DT)
        V.extend(DV)
        ```

Figure 1. Active learning with uncertainty sam
# [TransBTS paper](https://arxiv.org/pdf/2103.04430.pdf)

Made up job description

## Required skills
- familiarity with DNA mutations
- DNA sequiencing
- PCR
- PCA
- reusable data pipelines and SciKit-Learn Pipeline objects
- transformer
- `data table` vs Dask DataFrame
- Pipenv & Poetry
- setuptools and Anaconda
- Convert dosages to billable units
- Submit billing data to insurance providers
- Work claims and claim denials to ensure maximum reimbursement for services provided
- Perform Medicare reviews and audits
- Medicaid billing
- HIPPA certification
- FERPA compliance
- familiarity with digital ocean
- CVEs and Cybersecurity

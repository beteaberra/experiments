## Business Objective

We are building a system that helps professional translators and machine translators work together to efficiently and accurately translate technical medical documents from Spanish into English.

## Your Objective

Assist our translators in identifying words that are likely to be mistranslated by a machine so that they can be reviewed and corrected, if need be.

## Dataset

#### [glossary.yml](./glossary.yml)
Pairs of Spanish to English translations that machine translators (e.g. Google Translate) got incorrect and required correction by our professional translators.

#### [medical-text.es.txt](./medical-text.es.txt)
An example Spanish text that we would like to translate accurately into English accurately with as little correction from human translators as possible. 

## Your Code
Provide Python code and answers to these questions in a Jupyter notebook.

1. Split the example text into sentences
2. Write a python function to find all glossary terms in the example medical text and insert curly-brace ({}) markers around all Spanish terms from the glossary
3. Write a python function to replace all curly-brace delimited terms in the example medical text with glossary terms with their
4. Describe at least 2 approaches for generalize from these examples to create an algorithm to mark additional words for translators to review. You may use any natural language processing tools or pretrained deep learning models you like.
5. Implement one of your approaches from question 4 in Python and apply it to the example Spanish text provided. Your code should mark additional words in the text with curly braces as in step 2 of this exercise.


""" Scrape openssourcelibs.com """
import requests

from bs4 import BeautifulSoup
import pandas as pd


def scrape(
        url='https://opensourcelibs.com/libs',
        category='pytorch-implementation',
        name_class='flex-apart',
        desc_class='small-desc',
        save=True):

    url = f"{url.rstrip('/')}/{category}"
    resp = requests.get(url)
    html = resp.text  # text = resp.content.decode()
    soup = BeautifulSoup(html, 'html.parser')
    # first_xpath = '/html/body/div/div/div/div/div/div[6]/div[1]'
    descriptions = soup.find_all('div', class_=desc_class)
    names = soup.find_all('div', class_=name_class)
    if len(names) != len(descriptions):
        print(f'len(names) != len(descriptions):\n{len(names)} != {len(descriptions)}')
    projects = []
    for (n, d) in zip(names, descriptions):
        p = dict(
            desc=d.text,
            full_name=n.text)
        try:
            p.update(
                dict(
                    name=n.span.text)
            )
        except Exception as e:
            print(e)
        try:
            p.update(
                dict(
                    stars=int(n.text[len(n.span.text):].strip().split()[0])
                ))
        except Exception as e:
            print(e)
        projects.append(p)
    df = pd.DataFrame(projects)
    if save:
        df.to_csv(open(f'opensourcelibs-{category}-{len(df)}.csv', 'at'))
    return df


if __name__ == '__main__':
    df = scrape()

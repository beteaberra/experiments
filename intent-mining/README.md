# Intent Mining

## Milestone 1 (warmup) - Data Cleaning and Initial Model

**High-level overview** : A labeled and unlabeled dataset + classification model.  

### Tasks
* [DONE] Create a DataFrame for a labeled dataset from a Wit.AI export file <insert URL of the file utterances.json> and save it to `data/` directory as csv. `columns=['utterance', 'intent']`(str) - Estimated time: 4hrs; Actual time: 1hr
* [DONE] Create a Dataframe for an unlabeled dataset from a the 'Unlabeled Utterances" section of Maya NLP <url to the .csv file> and save it to `data/` directory as csv. Estimated time: 10min; Actual time: 10min
* [DONE] Concatenate both DataFrames together vertically (`axis=0`); the unlabeled utterances should have an empty string in the `intent` column and save it to `data/` as csv. Estimated time: 10min; Actual time: 10min
* [DONE] Create a dataframe of Spacy embeddings and the intent label. (301 columns) and save it to `data/` directory as csv.
  * Find library that gives simple API sentence embeddings (FastText); Timebox period: 1 hr
* Train logistic regression to predict the labeled intents and unlabeled intents.
  * Create the X (data), y (labels) variables from the labeled dataset.
  * Split your labeled dataset using SciKitLearn's `train_test_split` (80% train - 20% test)
  * Use `LogisticRegression` to predict the label of each utterance
* Provide a report on the accuracy of that model.

## Milestone 2 - Clustering 

### Tasks
* Use a K-means or DBScan algorithm to perform clustering of the utterances based on their embedding.
* Compare between the clustering by the algorithm using labeled dataset and Rand index.
* Add the unlabeled data to clustering dataset and perform the evaluation again. 


## Milestone 3 - FastText embeddings 
* Embed the utterances using FastText embeddings here: 
* Perform and evaluate the clustering as done in Milestone 2

## Milestone 4 - Nepali


## Features
- 

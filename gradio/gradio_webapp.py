# gradio_poly
import gradio
from qary.chat import v3
import logging

log = logging.getLogger(__name__)


def greet(user_text='', state={}):
    if user_text.startswith('My name is'):
        state['name'] = ''.join(c for c in user_text[10:].split()[0] if c not in '!.+=,;"[]{}')
    return f"Hello {state.get('name', 'Human')}!", state


def poly_reply(user_text='', state={}):
    log.info(f'user_text: {user_text}')
    log.info(f'state: {state}')
    state = v3.next_state(user_text, **state)
    return (
        state.get('bot_text', 'ERROR: not bot_text returned from v3.Engine()'),
        state
    )


def echo(user_text='', state={}):
    return (user_text, state)


if __name__ == '__main__':
    iface = gradio.Interface(fn=echo, inputs=["text", "state"], outputs=["text", "state"])
    iface.launch(share=True)

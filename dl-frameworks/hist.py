import pandas as pd
import re

dfs = pd.read_html('https://en.wikipedia.org/wiki/Comparison_of_deep-learning_software')
df = dfs[0]
df

# clean up binary columns
bincols = list(df.loc[:, 'OpenMP support':].columns)
bincols += ['Open source', 'Platform', 'Interface']
dfd = {}
for i, row in df.iterrows():
    rowd = row.fillna('No').to_dict()
    for c in bincols:
        text = str(rowd[c]).strip().lower()
        tokens = re.split(r'\W+', text)
        tokens += '*'
        rowd[c] = 0
        for kw, score in zip(
                'yes via roadmap no linux android python *'.split(),
                [1, .9, .2, 0, 2, 2, 2, .1]):
            if kw in tokens:
                rowd[c] = score
                break
    dfd[i] = rowd

# create dataframe from dict of dicts
df = pd.DataFrame(dfd).T
scores = df[bincols].T.sum()
df['Portability'] = scores
df = df.sort_values('Portability', ascending=False)

# actively developed, open source, supports Linux, python API:
df = df.reset_index()
print(df[['Software', 'Portability']][:10])

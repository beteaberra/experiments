from osmapi import OsmApi


api = OsmApi(
    api="api06.dev.openstreetmap.org",
    username="hobs",
    passwordfile="/home/hobs/.ssh/.env/osm.env",
    changesetauto=True,
    changesetautotags={"comment": u"changeset auto"},
    changesetautosize=1000,
    changesetautomulti=10
)

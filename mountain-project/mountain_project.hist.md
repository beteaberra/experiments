>>> >>> from osmapi import OsmApi
... >>> MyApi = OsmApi()
... >>> print(MyApi.NodeGet(123))
...
>>> api = OsmApi(api="api06.dev.openstreetmap.org", username="hobs", passwordfile="/home/hobs/.ssh/.env/osm.env", changesetauto=True, changesetautotags={"comment":u"changeset auto"}, changesetautosize=1000, changesetautomulti=10)
>>> api?
>>> api = OsmApi(api="api06.dev.openstreetmap.org", username="hobs", passwordfile="/home/hobs/.ssh/.env/osm.env", changesetauto=True, changesetautotags={"comment":u"changeset auto"}, changesetautosize=1000, changesetautomulti=10)
>>> hist
>>> dir(api)
['Capabilities',
 'Changeset',
 'ChangesetClose',
 'ChangesetComment',
 'ChangesetCreate',
 'ChangesetDownload',
 'ChangesetGet',
 'ChangesetSubscribe',
 'ChangesetUnsubscribe',
 'ChangesetUpdate',
 'ChangesetUpload',
 'ChangesetsGet',
 'Map',
 'NodeCreate',
 'NodeDelete',
 'NodeGet',
 'NodeHistory',
 'NodeRelations',
 'NodeUpdate',
 'NodeWays',
 'NodesGet',
 'NoteClose',
 'NoteComment',
 'NoteCreate',
 'NoteGet',
 'NoteReopen',
 'NotesGet',
 'NotesSearch',
 'RelationCreate',
 'RelationDelete',
 'RelationFull',
 'RelationFullRecur',
 'RelationGet',
 'RelationHistory',
 'RelationRelations',
 'RelationUpdate',
 'RelationsGet',
 'WayCreate',
 'WayDelete',
 'WayFull',
 'WayGet',
 'WayHistory',
 'WayRelations',
 'WayUpdate',
 'WaysGet',
 '_CurrentChangesetId',
 '_NoteAction',
 '__class__',
 '__delattr__',
 '__dict__',
 '__dir__',
 '__doc__',
 '__enter__',
 '__eq__',
 '__exit__',
 '__format__',
 '__ge__',
 '__getattribute__',
 '__gt__',
 '__hash__',
 '__init__',
 '__init_subclass__',
 '__le__',
 '__lt__',
 '__module__',
 '__ne__',
 '__new__',
 '__reduce__',
 '__reduce_ex__',
 '__repr__',
 '__setattr__',
 '__sizeof__',
 '__str__',
 '__subclasshook__',
 '__weakref__',
 '_api',
 '_changesetauto',
 '_changesetautocpt',
 '_changesetautodata',
 '_changesetautoflush',
 '_changesetautomulti',
 '_changesetautosize',
 '_changesetautotags',
 '_created_by',
 '_do',
 '_do_manu',
 '_password',
 '_session',
 '_username',
 'close',
 'flush',
 'http_session']
>>> api._password
'LR5PP5imBQ5F8uRCEHA8p'
>>> api._username
'hobs'
>>> api.http_session?
>>> df
>>> df = pd.read_csv('/home/hobs/code/tangibleai/mountain-project/mountain-project-boulder-san-diego-county.csv')
>>> import pandas as pd
>>> df = pd.read_csv('/home/hobs/code/tangibleai/mountain-project/mountain-project-boulder-san-diego-county.csv')
>>> df
               Route                                           Location                                                URL  ...  Length  Area Latitude Area Longitude
0       Baby Robbins  Jaws Vicinity > Jaws Boulder Area > Mount Wood...  https://www.mountainproject.com/route/10579165...  ...    15.0       33.00579     -116.96748
1    Masochist Crack  Beehive Boulders > Moby Dick Area > Santee Bou...  https://www.mountainproject.com/route/10593039...  ...    10.0       32.85097     -117.02078
2           Big Horn  The Cave Area > Mount Woodson > North San Dieg...  https://www.mountainproject.com/route/10580366...  ...    12.0       33.00766     -116.96944
3    Hamburger Crack  Hamburger Crack Area > Mount Woodson > North S...  https://www.mountainproject.com/route/10579640...  ...     9.0       33.00713     -116.96811
4          The Knobs  Beehive Boulders > Moby Dick Area > Santee Bou...  https://www.mountainproject.com/route/10599298...  ...    10.0       32.85097     -117.02078
..               ...                                                ...                                                ...  ...     ...            ...            ...
335      Pillow Talk  Hanging Garden area > Mount Woodson > North Sa...  https://www.mountainproject.com/route/11192169...  ...     NaN       33.01125     -116.96287
336    Little Lizard  Julian > North San Diego County > San Diego Co...  https://www.mountainproject.com/route/11134860...  ...    10.0       33.09827     -116.66480
337       Culptastic  Culp Boulder > CVPC > Culp Valley > San Diego ...  https://www.mountainproject.com/route/10888894...  ...    12.0       33.22363     -116.45271
338       Fresh Culp  Culp Boulder > CVPC > Culp Valley > San Diego ...  https://www.mountainproject.com/route/10888894...  ...    10.0       33.22363     -116.45271
339       Down climb  Border Bowls Boulder > Headbanger Cove > Valle...  https://www.mountainproject.com/route/11253421...  ...    12.0       32.62257     -116.07072

[340 rows x 11 columns]
>>> locs = df['Location'].str.split('>')
>>> locs
0      [Jaws Vicinity ,  Jaws Boulder Area ,  Mount W...
1      [Beehive Boulders ,  Moby Dick Area ,  Santee ...
2      [The Cave Area ,  Mount Woodson ,  North San D...
3      [Hamburger Crack Area ,  Mount Woodson ,  Nort...
4      [Beehive Boulders ,  Moby Dick Area ,  Santee ...
                             ...                        
335    [Hanging Garden area ,  Mount Woodson ,  North...
336    [Julian ,  North San Diego County ,  San Diego...
337    [Culp Boulder ,  CVPC ,  Culp Valley ,  San Di...
338    [Culp Boulder ,  CVPC ,  Culp Valley ,  San Di...
339    [Border Bowls Boulder ,  Headbanger Cove ,  Va...
Name: Location, Length: 340, dtype: object
>>> df = df.sort_values('Location')
>>> df
                 Route                                           Location                                                URL  ...  Length  Area Latitude Area Longitude
294          DownClimb  "A" Boulder > Vector Crack Area > Cottonwood A...  https://www.mountainproject.com/route/10697243...  ...     NaN       33.21073     -116.45935
142  South Face Plates  "B" Boulder > Windy Boulders South > Windy Bou...  https://www.mountainproject.com/route/11779496...  ...    10.0       33.21294     -116.45544
155          East Face  "B" Boulder > Windy Boulders South > Windy Bou...  https://www.mountainproject.com/route/11779498...  ...    10.0       33.21294     -116.45544
305    Chicken Chunder  **West Boulders > Descanso Boulders > Descanso...  https://www.mountainproject.com/route/11655752...  ...    12.0       32.84439     -116.64371
191             Nagual  Aztec Creek > South San Diego County > San Die...  https://www.mountainproject.com/route/11834226...  ...     9.0       32.77869     -117.06531
..                 ...                                                ...                                                ...  ...     ...            ...            ...
295       Camp Warm Up  Warm Up Boulder > Cottonwood Camp > Cottonwood...  https://www.mountainproject.com/route/12134974...  ...     9.0       33.21328     -116.46109
65        Slab warm-up  Warm-up Boulder > Sandbag Deluxe Circuit > Cul...  https://www.mountainproject.com/route/11243183...  ...    18.0       33.22042     -116.44981
164           Traverse  Wash Board Boulder > Sandbag Deluxe Circuit > ...  https://www.mountainproject.com/route/11243193...  ...    30.0       33.21803     -116.44553
206           Lay Back  West Bernardo Hills > North San Diego County >...  https://www.mountainproject.com/route/11282706...  ...    11.0       33.04988     -117.09425
36   Where Eagles Dare  Where Eagles Dare > Rancho Penasquitos Canyon ...  https://www.mountainproject.com/route/10810535...  ...     NaN       32.92710     -117.17720

[340 rows x 11 columns]
>>> locs = pd.DataFrame([x[:3] for x in locs])
>>> locs
                         0                         1                         2
0           Jaws Vicinity         Jaws Boulder Area             Mount Woodson 
1        Beehive Boulders            Moby Dick Area           Santee Boulders 
2           The Cave Area             Mount Woodson    North San Diego County 
3    Hamburger Crack Area             Mount Woodson    North San Diego County 
4        Beehive Boulders            Moby Dick Area           Santee Boulders 
..                     ...                       ...                       ...
335   Hanging Garden area             Mount Woodson    North San Diego County 
336                Julian    North San Diego County          San Diego County 
337          Culp Boulder                      CVPC               Culp Valley 
338          Culp Boulder                      CVPC               Culp Valley 
339  Border Bowls Boulder           Headbanger Cove        Valley of the Moon 

[340 rows x 3 columns]
>>> locs = pd.DataFrame([x[:4] for x in locs])
>>> locs = pd.DataFrame([[s.strip() for s in row] for row in df['Location'].str.split('>')])
>>> locs
                       0                          1                       2                             3                             4                 5           6
0            "A" Boulder          Vector Crack Area         Cottonwood Area                   Culp Valley  San Diego County Desert Area  San Diego County  California
1            "B" Boulder       Windy Boulders South          Windy Boulders                   Culp Valley  San Diego County Desert Area  San Diego County  California
2            "B" Boulder       Windy Boulders South          Windy Boulders                   Culp Valley  San Diego County Desert Area  San Diego County  California
3        **West Boulders          Descanso Boulders           Descanso Wall        South San Diego County              San Diego County        California        None
4            Aztec Creek     South San Diego County        San Diego County                    California                          None              None        None
..                   ...                        ...                     ...                           ...                           ...               ...         ...
335      Warm Up Boulder            Cottonwood Camp         Cottonwood Area                   Culp Valley  San Diego County Desert Area  San Diego County  California
336      Warm-up Boulder     Sandbag Deluxe Circuit             Culp Valley  San Diego County Desert Area              San Diego County        California        None
337   Wash Board Boulder     Sandbag Deluxe Circuit             Culp Valley  San Diego County Desert Area              San Diego County        California        None
338  West Bernardo Hills     North San Diego County        San Diego County                    California                          None              None        None
339    Where Eagles Dare  Rancho Penasquitos Canyon  North San Diego County              San Diego County                    California              None        None

[340 rows x 7 columns]
>>> locs = pd.DataFrame([[s.strip() for s in reversed(row)] for row in df['Location'].str.split('>')])
>>> locs
              0                 1                             2                          3                       4                     5                6
0    California  San Diego County  San Diego County Desert Area                Culp Valley         Cottonwood Area     Vector Crack Area      "A" Boulder
1    California  San Diego County  San Diego County Desert Area                Culp Valley          Windy Boulders  Windy Boulders South      "B" Boulder
2    California  San Diego County  San Diego County Desert Area                Culp Valley          Windy Boulders  Windy Boulders South      "B" Boulder
3    California  San Diego County        South San Diego County              Descanso Wall       Descanso Boulders       **West Boulders             None
4    California  San Diego County        South San Diego County                Aztec Creek                    None                  None             None
..          ...               ...                           ...                        ...                     ...                   ...              ...
335  California  San Diego County  San Diego County Desert Area                Culp Valley         Cottonwood Area       Cottonwood Camp  Warm Up Boulder
336  California  San Diego County  San Diego County Desert Area                Culp Valley  Sandbag Deluxe Circuit       Warm-up Boulder             None
337  California  San Diego County  San Diego County Desert Area                Culp Valley  Sandbag Deluxe Circuit    Wash Board Boulder             None
338  California  San Diego County        North San Diego County        West Bernardo Hills                    None                  None             None
339  California  San Diego County        North San Diego County  Rancho Penasquitos Canyon       Where Eagles Dare                  None             None

[340 rows x 7 columns]
>>> locs.columns = 'state county county_area area subarea group boulder'.split()
>>> pd.concat([df, locs])
                 Route                                           Location  ...               group          boulder
294          DownClimb  "A" Boulder > Vector Crack Area > Cottonwood A...  ...                 NaN              NaN
142  South Face Plates  "B" Boulder > Windy Boulders South > Windy Bou...  ...                 NaN              NaN
155          East Face  "B" Boulder > Windy Boulders South > Windy Bou...  ...                 NaN              NaN
305    Chicken Chunder  **West Boulders > Descanso Boulders > Descanso...  ...                 NaN              NaN
191             Nagual  Aztec Creek > South San Diego County > San Die...  ...                 NaN              NaN
..                 ...                                                ...  ...                 ...              ...
335                NaN                                                NaN  ...     Cottonwood Camp  Warm Up Boulder
336                NaN                                                NaN  ...     Warm-up Boulder             None
337                NaN                                                NaN  ...  Wash Board Boulder             None
338                NaN                                                NaN  ...                None             None
339                NaN                                                NaN  ...                None             None

[680 rows x 18 columns]
>>> pd.concat([df, locs], axis=1)
               Route                                           Location  ...                 group          boulder
0       Baby Robbins  Jaws Vicinity > Jaws Boulder Area > Mount Wood...  ...     Vector Crack Area      "A" Boulder
1    Masochist Crack  Beehive Boulders > Moby Dick Area > Santee Bou...  ...  Windy Boulders South      "B" Boulder
2           Big Horn  The Cave Area > Mount Woodson > North San Dieg...  ...  Windy Boulders South      "B" Boulder
3    Hamburger Crack  Hamburger Crack Area > Mount Woodson > North S...  ...       **West Boulders             None
4          The Knobs  Beehive Boulders > Moby Dick Area > Santee Bou...  ...                  None             None
..               ...                                                ...  ...                   ...              ...
335      Pillow Talk  Hanging Garden area > Mount Woodson > North Sa...  ...       Cottonwood Camp  Warm Up Boulder
336    Little Lizard  Julian > North San Diego County > San Diego Co...  ...       Warm-up Boulder             None
337       Culptastic  Culp Boulder > CVPC > Culp Valley > San Diego ...  ...    Wash Board Boulder             None
338       Fresh Culp  Culp Boulder > CVPC > Culp Valley > San Diego ...  ...                  None             None
339       Down climb  Border Bowls Boulder > Headbanger Cove > Valle...  ...                  None             None

[340 rows x 18 columns]
>>> df = pd.concat([df, locs], axis=1)
>>> df.to_csv('~/code/tangibleai/experiments/mountain-project/boulder-san-diego-county.csv')
>>> ls
>>> cd mountain-project/
>>> ls -hal
>>> df
               Route                                           Location  ...                 group          boulder
0       Baby Robbins  Jaws Vicinity > Jaws Boulder Area > Mount Wood...  ...     Vector Crack Area      "A" Boulder
1    Masochist Crack  Beehive Boulders > Moby Dick Area > Santee Bou...  ...  Windy Boulders South      "B" Boulder
2           Big Horn  The Cave Area > Mount Woodson > North San Dieg...  ...  Windy Boulders South      "B" Boulder
3    Hamburger Crack  Hamburger Crack Area > Mount Woodson > North S...  ...       **West Boulders             None
4          The Knobs  Beehive Boulders > Moby Dick Area > Santee Bou...  ...                  None             None
..               ...                                                ...  ...                   ...              ...
335      Pillow Talk  Hanging Garden area > Mount Woodson > North Sa...  ...       Cottonwood Camp  Warm Up Boulder
336    Little Lizard  Julian > North San Diego County > San Diego Co...  ...       Warm-up Boulder             None
337       Culptastic  Culp Boulder > CVPC > Culp Valley > San Diego ...  ...    Wash Board Boulder             None
338       Fresh Culp  Culp Boulder > CVPC > Culp Valley > San Diego ...  ...                  None             None
339       Down climb  Border Bowls Boulder > Headbanger Cove > Valle...  ...                  None             None

[340 rows x 18 columns]
>>> df['county'].unique()
array(['San Diego County'], dtype=object)
>>> df['area'].unique()
array(['Culp Valley', 'Descanso Wall', 'Aztec Creek', 'Santee Boulders',
       'South Mt. Woodson', 'McCain Valley', 'Valley of the Moon',
       'Lake Ramona', 'Old Castle', 'Rancho Penasquitos Canyon',
       'Mt. Gower', 'Mount Woodson', 'MCB Camp Pendleton.',
       'Calavera Lake', 'Melrose Boulders', 'Magnolia Boulders', 'Julian',
       'La Jolla', 'Lost Valley', 'Mission Gorge', 'Daley Ranch',
       'Blair Valley', 'West Bernardo Hills'], dtype=object)
>>> df.columns
Index(['Route', 'Location', 'URL', 'Avg Stars', 'Your Stars', 'Route Type',
       'Rating', 'Pitches', 'Length', 'Area Latitude', 'Area Longitude',
       'state', 'county', 'county_area', 'area', 'subarea', 'group',
       'boulder'],
      dtype='object')
>>> df.head()
             Route                                           Location                                                URL  ...            subarea                 group      boulder
0     Baby Robbins  Jaws Vicinity > Jaws Boulder Area > Mount Wood...  https://www.mountainproject.com/route/10579165...  ...    Cottonwood Area     Vector Crack Area  "A" Boulder
1  Masochist Crack  Beehive Boulders > Moby Dick Area > Santee Bou...  https://www.mountainproject.com/route/10593039...  ...     Windy Boulders  Windy Boulders South  "B" Boulder
2         Big Horn  The Cave Area > Mount Woodson > North San Dieg...  https://www.mountainproject.com/route/10580366...  ...     Windy Boulders  Windy Boulders South  "B" Boulder
3  Hamburger Crack  Hamburger Crack Area > Mount Woodson > North S...  https://www.mountainproject.com/route/10579640...  ...  Descanso Boulders       **West Boulders         None
4        The Knobs  Beehive Boulders > Moby Dick Area > Santee Bou...  https://www.mountainproject.com/route/10599298...  ...               None                  None         None

[5 rows x 18 columns]
>>> df
               Route                                           Location  ...                 group          boulder
0       Baby Robbins  Jaws Vicinity > Jaws Boulder Area > Mount Wood...  ...     Vector Crack Area      "A" Boulder
1    Masochist Crack  Beehive Boulders > Moby Dick Area > Santee Bou...  ...  Windy Boulders South      "B" Boulder
2           Big Horn  The Cave Area > Mount Woodson > North San Dieg...  ...  Windy Boulders South      "B" Boulder
3    Hamburger Crack  Hamburger Crack Area > Mount Woodson > North S...  ...       **West Boulders             None
4          The Knobs  Beehive Boulders > Moby Dick Area > Santee Bou...  ...                  None             None
..               ...                                                ...  ...                   ...              ...
335      Pillow Talk  Hanging Garden area > Mount Woodson > North Sa...  ...       Cottonwood Camp  Warm Up Boulder
336    Little Lizard  Julian > North San Diego County > San Diego Co...  ...       Warm-up Boulder             None
337       Culptastic  Culp Boulder > CVPC > Culp Valley > San Diego ...  ...    Wash Board Boulder             None
338       Fresh Culp  Culp Boulder > CVPC > Culp Valley > San Diego ...  ...                  None             None
339       Down climb  Border Bowls Boulder > Headbanger Cove > Valle...  ...                  None             None

[340 rows x 18 columns]
>>> pd.options.display.max_columns
0
>>> pd.options.display.max_columns = None
>>> df
               Route                                           Location  \
0       Baby Robbins  Jaws Vicinity > Jaws Boulder Area > Mount Wood...   
1    Masochist Crack  Beehive Boulders > Moby Dick Area > Santee Bou...   
2           Big Horn  The Cave Area > Mount Woodson > North San Dieg...   
3    Hamburger Crack  Hamburger Crack Area > Mount Woodson > North S...   
4          The Knobs  Beehive Boulders > Moby Dick Area > Santee Bou...   
..               ...                                                ...   
335      Pillow Talk  Hanging Garden area > Mount Woodson > North Sa...   
336    Little Lizard  Julian > North San Diego County > San Diego Co...   
337       Culptastic  Culp Boulder > CVPC > Culp Valley > San Diego ...   
338       Fresh Culp  Culp Boulder > CVPC > Culp Valley > San Diego ...   
339       Down climb  Border Bowls Boulder > Headbanger Cove > Valle...   

                                                   URL  Avg Stars  Your Stars  \
0    https://www.mountainproject.com/route/10579165...        2.7          -1   
1    https://www.mountainproject.com/route/10593039...        2.6          -1   
2    https://www.mountainproject.com/route/10580366...        2.8          -1   
3    https://www.mountainproject.com/route/10579640...        2.1          -1   
4    https://www.mountainproject.com/route/10599298...        2.1          -1   
..                                                 ...        ...         ...   
335  https://www.mountainproject.com/route/11192169...        0.6          -1   
336  https://www.mountainproject.com/route/11134860...        0.7          -1   
337  https://www.mountainproject.com/route/10888894...        0.5          -1   
338  https://www.mountainproject.com/route/10888894...        0.3          -1   
339  https://www.mountainproject.com/route/11253421...       -1.0          -1   

        Route Type       Rating  Pitches  Length  Area Latitude  \
0    Trad, Boulder      5.9 V0-        1    15.0       33.00579   
1    Trad, Boulder      5.9 V0-        1    10.0       32.85097   
2    Trad, Boulder     5.8+ V0-        1    12.0       33.00766   
3    Trad, Boulder       5.9 V0        1     9.0       33.00713   
4          Boulder          V0-        1    10.0       32.85097   
..             ...          ...      ...     ...            ...   
335        Boulder           V0        1     NaN       33.01125   
336        Boulder  V-easy PG13        1    10.0       33.09827   
337        Boulder          V0-        1    12.0       33.22363   
338        Boulder       V-easy        1    10.0       33.22363   
339        Boulder       V-easy        1    12.0       32.62257   

     Area Longitude       state            county  \
0        -116.96748  California  San Diego County   
1        -117.02078  California  San Diego County   
2        -116.96944  California  San Diego County   
3        -116.96811  California  San Diego County   
4        -117.02078  California  San Diego County   
..              ...         ...               ...   
335      -116.96287  California  San Diego County   
336      -116.66480  California  San Diego County   
337      -116.45271  California  San Diego County   
338      -116.45271  California  San Diego County   
339      -116.07072  California  San Diego County   

                      county_area                       area  \
0    San Diego County Desert Area                Culp Valley   
1    San Diego County Desert Area                Culp Valley   
2    San Diego County Desert Area                Culp Valley   
3          South San Diego County              Descanso Wall   
4          South San Diego County                Aztec Creek   
..                            ...                        ...   
335  San Diego County Desert Area                Culp Valley   
336  San Diego County Desert Area                Culp Valley   
337  San Diego County Desert Area                Culp Valley   
338        North San Diego County        West Bernardo Hills   
339        North San Diego County  Rancho Penasquitos Canyon   

                    subarea                 group          boulder  
0           Cottonwood Area     Vector Crack Area      "A" Boulder  
1            Windy Boulders  Windy Boulders South      "B" Boulder  
2            Windy Boulders  Windy Boulders South      "B" Boulder  
3         Descanso Boulders       **West Boulders             None  
4                      None                  None             None  
..                      ...                   ...              ...  
335         Cottonwood Area       Cottonwood Camp  Warm Up Boulder  
336  Sandbag Deluxe Circuit       Warm-up Boulder             None  
337  Sandbag Deluxe Circuit    Wash Board Boulder             None  
338                    None                  None             None  
339       Where Eagles Dare                  None             None  

[340 rows x 18 columns]
>>> df['Rating'].str.contains('V0')
0       True
1       True
2       True
3       True
4       True
       ...  
335     True
336    False
337     True
338    False
339    False
Name: Rating, Length: 340, dtype: bool
>>> df['V0'] = df['Rating'].str.contains('V0')
>>> df['5_9'] = df['Rating'].str.contains('5.9')
>>> df['V1'] = df['Rating'].str.contains('V1')
>>> df['5_10'] = df['Rating'].str.contains('5.10')
>>> df.sort_values('V0', ascending=False)
                        Route  \
0                Baby Robbins   
208                      Slab   
216                 Low Rider   
215                     Tower   
213  Unknown Black Face/Arete   
..                        ...   
71                 East crack   
244              Crack Direct   
242          Southwest Corner   
76                  Seam Slab   
339                Down climb   

                                              Location  \
0    Jaws Vicinity > Jaws Boulder Area > Mount Wood...   
208  The Boot > Hilltop Boulders > CVPC > Culp Vall...   
216  Hill Top area > Hill Top > Melrose Boulders > ...   
215  Need a Name > Jasper Trail Boulders > Culp Val...   
213  Pile Unknown > Dog Pile Area > Santee Boulders...   
..                                                 ...   
71   East Crack Boulder > Melrose Boulders > North ...   
244  Round Rock > Dog Pile Area > Santee Boulders >...   
242  Jim Thoen Boulder > Melrose Boulders > North S...   
76   The Dog Pile > Dog Pile Area > Santee Boulders...   
339  Border Bowls Boulder > Headbanger Cove > Valle...   

                                                   URL  Avg Stars  Your Stars  \
0    https://www.mountainproject.com/route/10579165...        2.7          -1   
208  https://www.mountainproject.com/route/11243208...        2.0          -1   
216  https://www.mountainproject.com/route/10869398...        2.0          -1   
215  https://www.mountainproject.com/route/11244821...        2.0          -1   
213  https://www.mountainproject.com/route/11009703...        2.0          -1   
..                                                 ...        ...         ...   
71   https://www.mountainproject.com/route/10858367...        2.8          -1   
244  https://www.mountainproject.com/route/11702332...        1.4          -1   
242  https://www.mountainproject.com/route/10864083...        1.7          -1   
76   https://www.mountainproject.com/route/12024702...        2.6          -1   
339  https://www.mountainproject.com/route/11253421...       -1.0          -1   

        Route Type      Rating  Pitches  Length  Area Latitude  \
0    Trad, Boulder     5.9 V0-        1    15.0       33.00579   
208        Boulder          V0        1    20.0       33.22256   
216        Boulder         V0+        1    10.0       33.22432   
215        Boulder         V0-        1    20.0       33.20313   
213        Boulder     V0 PG13        1    15.0       32.85020   
..             ...         ...      ...     ...            ...   
71         Boulder  5.6 V-easy        1    15.0       33.22432   
244    TR, Boulder  5.7 V-easy        1    15.0       32.85063   
242        Boulder      V-easy        1    12.0       33.22432   
76         Boulder  5.5 V-easy        1    15.0       32.85048   
339        Boulder      V-easy        1    12.0       32.62257   

     Area Longitude       state            county  \
0        -116.96748  California  San Diego County   
208      -116.45287  California  San Diego County   
216      -117.25797  California  San Diego County   
215      -116.48854  California  San Diego County   
213      -117.02333  California  San Diego County   
..              ...         ...               ...   
71       -117.25797  California  San Diego County   
244      -117.02294  California  San Diego County   
242      -117.25797  California  San Diego County   
76       -117.02311  California  San Diego County   
339      -116.07072  California  San Diego County   

                      county_area                       area  \
0    San Diego County Desert Area                Culp Valley   
208        North San Diego County                  Mt. Gower   
216  San Diego County Desert Area                Culp Valley   
215  San Diego County Desert Area                Culp Valley   
213  San Diego County Desert Area                Culp Valley   
..                            ...                        ...   
71         North San Diego County  Rancho Penasquitos Canyon   
244        South San Diego County          Magnolia Boulders   
242        South San Diego County          Magnolia Boulders   
76         North San Diego County  Rancho Penasquitos Canyon   
339        North San Diego County  Rancho Penasquitos Canyon   

                    subarea                 group           boulder     V0  \
0           Cottonwood Area     Vector Crack Area       "A" Boulder   True   
208     Quiet Oaks Boulders                  None              None   True   
216  Sandbag Deluxe Circuit    Rock Stars Boulder              None   True   
215  Sandbag Deluxe Circuit    Rock Stars Boulder              None   True   
213          Windy Boulders  Windy Boulders North  Roadview Boulder   True   
..                      ...                   ...               ...    ...   
71            Del Poconitos                  None              None  False   
244             South slope                  None              None  False   
242             South slope                  None              None  False   
76           Southern Slope      Disguise Boulder              None  False   
339       Where Eagles Dare                  None              None  False   

       5_9     V1   5_10  
0     True  False  False  
208  False  False  False  
216  False  False  False  
215  False  False  False  
213  False  False  False  
..     ...    ...    ...  
71   False  False  False  
244  False  False  False  
242  False  False  False  
76   False  False  False  
339  False  False  False  

[340 rows x 22 columns]
>>> df = df.sort_values('V0', ascending=False)
>>> df.head(10)
                        Route  \
0                Baby Robbins   
208                      Slab   
216                 Low Rider   
215                     Tower   
213  Unknown Black Face/Arete   
212            Quartzite Line   
211                 East face   
210                 Xeno Face   
209       East arete traverse   
207       Fingers for the Win   

                                              Location  \
0    Jaws Vicinity > Jaws Boulder Area > Mount Wood...   
208  The Boot > Hilltop Boulders > CVPC > Culp Vall...   
216  Hill Top area > Hill Top > Melrose Boulders > ...   
215  Need a Name > Jasper Trail Boulders > Culp Val...   
213  Pile Unknown > Dog Pile Area > Santee Boulders...   
212  Culp Tower > CVPC > Culp Valley > San Diego Co...   
211  Crystalsplit Boulder > Sandbag Deluxe Circuit ...   
210  Magic School Bus Area > Culp Valley > San Dieg...   
209  Crystalsplit Boulder > Sandbag Deluxe Circuit ...   
207  Lark Overlook Rock > McCain Valley > South San...   

                                                   URL  Avg Stars  Your Stars  \
0    https://www.mountainproject.com/route/10579165...        2.7          -1   
208  https://www.mountainproject.com/route/11243208...        2.0          -1   
216  https://www.mountainproject.com/route/10869398...        2.0          -1   
215  https://www.mountainproject.com/route/11244821...        2.0          -1   
213  https://www.mountainproject.com/route/11009703...        2.0          -1   
212  https://www.mountainproject.com/route/10920678...        2.0          -1   
211  https://www.mountainproject.com/route/11243167...        2.0          -1   
210  https://www.mountainproject.com/route/12189939...        2.0          -1   
209  https://www.mountainproject.com/route/11243166...        2.0          -1   
207  https://www.mountainproject.com/route/11980529...        2.0          -1   

        Route Type    Rating  Pitches  Length  Area Latitude  Area Longitude  \
0    Trad, Boulder   5.9 V0-        1    15.0       33.00579      -116.96748   
208        Boulder        V0        1    20.0       33.22256      -116.45287   
216        Boulder       V0+        1    10.0       33.22432      -117.25797   
215        Boulder       V0-        1    20.0       33.20313      -116.48854   
213        Boulder   V0 PG13        1    15.0       32.85020      -117.02333   
212  Trad, Boulder  5.10a V0        1    30.0       33.22406      -116.45214   
211        Boulder        V0        1    15.0       33.21864      -116.44096   
210        Boulder        V0        1    10.0       33.20598      -116.46571   
209        Boulder        V0        1    12.0       33.21864      -116.44096   
207        Boulder        V0        1    15.0       32.72481      -116.27347   

          state            county                   county_area         area  \
0    California  San Diego County  San Diego County Desert Area  Culp Valley   
208  California  San Diego County        North San Diego County    Mt. Gower   
216  California  San Diego County  San Diego County Desert Area  Culp Valley   
215  California  San Diego County  San Diego County Desert Area  Culp Valley   
213  California  San Diego County  San Diego County Desert Area  Culp Valley   
212  California  San Diego County  San Diego County Desert Area  Culp Valley   
211  California  San Diego County  San Diego County Desert Area  Culp Valley   
210  California  San Diego County  San Diego County Desert Area  Culp Valley   
209  California  San Diego County  San Diego County Desert Area  Culp Valley   
207  California  San Diego County  San Diego County Desert Area  Culp Valley   

                    subarea                     group                 boulder  \
0           Cottonwood Area         Vector Crack Area             "A" Boulder   
208     Quiet Oaks Boulders                      None                    None   
216  Sandbag Deluxe Circuit        Rock Stars Boulder                    None   
215  Sandbag Deluxe Circuit        Rock Stars Boulder                    None   
213          Windy Boulders      Windy Boulders North        Roadview Boulder   
212                    CVPC        Risk Taker Boulder                    None   
211                    CVPC  Northern Area Bouldering  Richard's Face Boulder   
210                    CVPC  Northern Area Bouldering       Red Baron Boulder   
209             Random Area          Random Boulder 2                    None   
207                    CVPC  Northern Area Bouldering   Quartz Stripe Boulder   

       V0    5_9     V1   5_10  
0    True   True  False  False  
208  True  False  False  False  
216  True  False  False  False  
215  True  False  False  False  
213  True  False  False  False  
212  True  False  False   True  
211  True  False  False  False  
210  True  False  False  False  
209  True  False  False  False  
207  True  False  False  False  
>>> df.sort_values('Stars')
>>> df.sort_values('Avg Stars')
                Route                                           Location  \
339        Down climb  Border Bowls Boulder > Headbanger Cove > Valle...   
338        Fresh Culp  Culp Boulder > CVPC > Culp Valley > San Diego ...   
337        Culptastic  Culp Boulder > CVPC > Culp Valley > San Diego ...   
335       Pillow Talk  Hanging Garden area > Mount Woodson > North Sa...   
336     Little Lizard  Julian > North San Diego County > San Diego Co...   
..                ...                                                ...   
20        Greg's Face  Seminar Wall Area > Mount Woodson > North San ...   
21         Posh Belom  Boulder Row > Lake Ramona > North San Diego Co...   
22   Robinson Project  The Octagon South Face > The Octagon Area > Bl...   
24     Chesty’s crack  Combat Town > MCB Camp Pendleton. > North San ...   
23    Ten Pound Brown  Ten Pound Brown > Blair Valley Campground Area...   

                                                   URL  Avg Stars  Your Stars  \
339  https://www.mountainproject.com/route/11253421...       -1.0          -1   
338  https://www.mountainproject.com/route/10888894...        0.3          -1   
337  https://www.mountainproject.com/route/10888894...        0.5          -1   
335  https://www.mountainproject.com/route/11192169...        0.6          -1   
336  https://www.mountainproject.com/route/11134860...        0.7          -1   
..                                                 ...        ...         ...   
20   https://www.mountainproject.com/route/10802947...        4.0          -1   
21   https://www.mountainproject.com/route/11869567...        4.0          -1   
22   https://www.mountainproject.com/route/12067033...        4.0          -1   
24   https://www.mountainproject.com/route/12223825...        4.0          -1   
23   https://www.mountainproject.com/route/12067844...        4.0          -1   

    Route Type       Rating  Pitches  Length  Area Latitude  Area Longitude  \
339    Boulder       V-easy        1    12.0       32.62257      -116.07072   
338    Boulder       V-easy        1    10.0       33.22363      -116.45271   
337    Boulder          V0-        1    12.0       33.22363      -116.45271   
335    Boulder           V0        1     NaN       33.01125      -116.96287   
336    Boulder  V-easy PG13        1    10.0       33.09827      -116.66480   
..         ...          ...      ...     ...            ...             ...   
20     Boulder          V0+        1    22.0       33.00660      -116.96539   
21     Boulder           V0        1    15.0       33.03135      -116.99701   
22     Boulder       V-easy        1    18.0       33.03646      -116.39782   
24     Boulder      5.9+ V0        1     8.0       33.34522      -117.35163   
23     Boulder           V0        1    16.0       33.03471      -116.39802   

          state            county                   county_area  \
339  California  San Diego County        North San Diego County   
338  California  San Diego County        North San Diego County   
337  California  San Diego County  San Diego County Desert Area   
335  California  San Diego County  San Diego County Desert Area   
336  California  San Diego County  San Diego County Desert Area   
..          ...               ...                           ...   
20   California  San Diego County        South San Diego County   
21   California  San Diego County  San Diego County Desert Area   
22   California  San Diego County  San Diego County Desert Area   
24   California  San Diego County        North San Diego County   
23   California  San Diego County        South San Diego County   

                          area                 subarea  \
339  Rancho Penasquitos Canyon       Where Eagles Dare   
338        West Bernardo Hills                    None   
337                Culp Valley  Sandbag Deluxe Circuit   
335                Culp Valley         Cottonwood Area   
336                Culp Valley  Sandbag Deluxe Circuit   
..                         ...                     ...   
20               McCain Valley       Cottonwood Giants   
21                 Culp Valley          Windy Boulders   
22                 Culp Valley          Windy Boulders   
24                 Lake Ramona             Boulder Row   
23          Valley of the Moon         Headbanger Cove   

                      group            boulder     V0    5_9     V1   5_10  
339                    None               None  False  False  False  False  
338                    None               None  False  False  False  False  
337      Wash Board Boulder               None   True  False  False  False  
335         Cottonwood Camp    Warm Up Boulder   True  False  False  False  
336         Warm-up Boulder               None  False  False  False  False  
..                      ...                ...    ...    ...    ...    ...  
20                    Big T               None   True  False  False  False  
21   Windy Boulders Central  Black Dot Boulder   True  False  False  False  
22   Windy Boulders Central  Black Dot Boulder  False  False  False  False  
24                     None               None   True   True  False  False  
23     Border Bowls Boulder               None   True  False  False  False  

[340 rows x 22 columns]
>>> df.sort_values('Avg Stars', ascending=False)
                Route                                           Location  \
22   Robinson Project  The Octagon South Face > The Octagon Area > Bl...   
21         Posh Belom  Boulder Row > Lake Ramona > North San Diego Co...   
20        Greg's Face  Seminar Wall Area > Mount Woodson > North San ...   
24     Chesty’s crack  Combat Town > MCB Camp Pendleton. > North San ...   
23    Ten Pound Brown  Ten Pound Brown > Blair Valley Campground Area...   
..                ...                                                ...   
336     Little Lizard  Julian > North San Diego County > San Diego Co...   
335       Pillow Talk  Hanging Garden area > Mount Woodson > North Sa...   
337        Culptastic  Culp Boulder > CVPC > Culp Valley > San Diego ...   
338        Fresh Culp  Culp Boulder > CVPC > Culp Valley > San Diego ...   
339        Down climb  Border Bowls Boulder > Headbanger Cove > Valle...   

                                                   URL  Avg Stars  Your Stars  \
22   https://www.mountainproject.com/route/12067033...        4.0          -1   
21   https://www.mountainproject.com/route/11869567...        4.0          -1   
20   https://www.mountainproject.com/route/10802947...        4.0          -1   
24   https://www.mountainproject.com/route/12223825...        4.0          -1   
23   https://www.mountainproject.com/route/12067844...        4.0          -1   
..                                                 ...        ...         ...   
336  https://www.mountainproject.com/route/11134860...        0.7          -1   
335  https://www.mountainproject.com/route/11192169...        0.6          -1   
337  https://www.mountainproject.com/route/10888894...        0.5          -1   
338  https://www.mountainproject.com/route/10888894...        0.3          -1   
339  https://www.mountainproject.com/route/11253421...       -1.0          -1   

    Route Type       Rating  Pitches  Length  Area Latitude  Area Longitude  \
22     Boulder       V-easy        1    18.0       33.03646      -116.39782   
21     Boulder           V0        1    15.0       33.03135      -116.99701   
20     Boulder          V0+        1    22.0       33.00660      -116.96539   
24     Boulder      5.9+ V0        1     8.0       33.34522      -117.35163   
23     Boulder           V0        1    16.0       33.03471      -116.39802   
..         ...          ...      ...     ...            ...             ...   
336    Boulder  V-easy PG13        1    10.0       33.09827      -116.66480   
335    Boulder           V0        1     NaN       33.01125      -116.96287   
337    Boulder          V0-        1    12.0       33.22363      -116.45271   
338    Boulder       V-easy        1    10.0       33.22363      -116.45271   
339    Boulder       V-easy        1    12.0       32.62257      -116.07072   

          state            county                   county_area  \
22   California  San Diego County  San Diego County Desert Area   
21   California  San Diego County  San Diego County Desert Area   
20   California  San Diego County        South San Diego County   
24   California  San Diego County        North San Diego County   
23   California  San Diego County        South San Diego County   
..          ...               ...                           ...   
336  California  San Diego County  San Diego County Desert Area   
335  California  San Diego County  San Diego County Desert Area   
337  California  San Diego County  San Diego County Desert Area   
338  California  San Diego County        North San Diego County   
339  California  San Diego County        North San Diego County   

                          area                 subarea  \
22                 Culp Valley          Windy Boulders   
21                 Culp Valley          Windy Boulders   
20               McCain Valley       Cottonwood Giants   
24                 Lake Ramona             Boulder Row   
23          Valley of the Moon         Headbanger Cove   
..                         ...                     ...   
336                Culp Valley  Sandbag Deluxe Circuit   
335                Culp Valley         Cottonwood Area   
337                Culp Valley  Sandbag Deluxe Circuit   
338        West Bernardo Hills                    None   
339  Rancho Penasquitos Canyon       Where Eagles Dare   

                      group            boulder     V0    5_9     V1   5_10  
22   Windy Boulders Central  Black Dot Boulder  False  False  False  False  
21   Windy Boulders Central  Black Dot Boulder   True  False  False  False  
20                    Big T               None   True  False  False  False  
24                     None               None   True   True  False  False  
23     Border Bowls Boulder               None   True  False  False  False  
..                      ...                ...    ...    ...    ...    ...  
336         Warm-up Boulder               None  False  False  False  False  
335         Cottonwood Camp    Warm Up Boulder   True  False  False  False  
337      Wash Board Boulder               None   True  False  False  False  
338                    None               None  False  False  False  False  
339                    None               None  False  False  False  False  

[340 rows x 22 columns]
>>> np = (32.7484460, -117.1301440)
>>> latlon = df['Latitude']
>>> df.columns
Index(['Route', 'Location', 'URL', 'Avg Stars', 'Your Stars', 'Route Type',
       'Rating', 'Pitches', 'Length', 'Area Latitude', 'Area Longitude',
       'state', 'county', 'county_area', 'area', 'subarea', 'group', 'boulder',
       'V0', '5_9', 'V1', '5_10'],
      dtype='object')
>>> latlon = df[['Area Latitude', 'Area Longitude']]
>>> latlon = df[('Area Latitude', 'Area Longitude')]
>>> latlon = df[['Area Latitude', 'Area Longitude']]
>>> latlon
     Area Latitude  Area Longitude
0         33.00579      -116.96748
208       33.22256      -116.45287
216       33.22432      -117.25797
215       33.20313      -116.48854
213       32.85020      -117.02333
..             ...             ...
71        33.22432      -117.25797
244       32.85063      -117.02294
242       33.22432      -117.25797
76        32.85048      -117.02311
339       32.62257      -116.07072

[340 rows x 2 columns]
>>> latlon - np.array(np)
>>> me = np
>>> import numpy as np
>>> latlon - np.array(np)
>>> latlon - np.array(me)
     Area Latitude  Area Longitude
0         0.257344        0.162664
208       0.474114        0.677274
216       0.475874       -0.127826
215       0.454684        0.641604
213       0.101754        0.106814
..             ...             ...
71        0.475874       -0.127826
244       0.102184        0.107204
242       0.475874       -0.127826
76        0.102034        0.107034
339      -0.125876        1.059424

[340 rows x 2 columns]
>>> md
>>> me
(32.748446, -117.130144)
>>> latlon[0]
>>> latlon.iloc[0]
Area Latitude      33.00579
Area Longitude   -116.96748
Name: 0, dtype: float64
>>> latlon.iloc[2]
Area Latitude      33.22432
Area Longitude   -117.25797
Name: 216, dtype: float64
>>> latlon.iloc[2] - me
Area Latitude     0.475874
Area Longitude   -0.127826
Name: 216, dtype: float64
>>> latlon - np.array(me)
     Area Latitude  Area Longitude
0         0.257344        0.162664
208       0.474114        0.677274
216       0.475874       -0.127826
215       0.454684        0.641604
213       0.101754        0.106814
..             ...             ...
71        0.475874       -0.127826
244       0.102184        0.107204
242       0.475874       -0.127826
76        0.102034        0.107034
339      -0.125876        1.059424

[340 rows x 2 columns]
>>> hist -f mountain_project.hist.py
>>> hist -o -p -f mountain_project.hist.md

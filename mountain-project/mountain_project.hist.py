>>> from osmapi import OsmApi
>>> MyApi = OsmApi()
>>> print(MyApi.NodeGet(123))
api = OsmApi(api="api06.dev.openstreetmap.org", username="hobs", passwordfile="/home/hobs/.ssh/.env/osm.env", changesetauto=True, changesetautotags={"comment":u"changeset auto"}, changesetautosize=1000, changesetautomulti=10)
api?
api = OsmApi(api="api06.dev.openstreetmap.org", username="hobs", passwordfile="/home/hobs/.ssh/.env/osm.env", changesetauto=True, changesetautotags={"comment":u"changeset auto"}, changesetautosize=1000, changesetautomulti=10)
hist
dir(api)
api._password
api._username
api.http_session?
df
df = pd.read_csv('/home/hobs/code/tangibleai/mountain-project/mountain-project-boulder-san-diego-county.csv')
import pandas as pd
df = pd.read_csv('/home/hobs/code/tangibleai/mountain-project/mountain-project-boulder-san-diego-county.csv')
df
locs = df['Location'].str.split('>')
locs
df = df.sort_values('Location')
df
locs = pd.DataFrame([x[:3] for x in locs])
locs
locs = pd.DataFrame([x[:4] for x in locs])
locs = pd.DataFrame([[s.strip() for s in row] for row in df['Location'].str.split('>')])
locs
locs = pd.DataFrame([[s.strip() for s in reversed(row)] for row in df['Location'].str.split('>')])
locs
locs.columns = 'state county county_area area subarea group boulder'.split()
pd.concat([df, locs])
pd.concat([df, locs], axis=1)
df = pd.concat([df, locs], axis=1)
df.to_csv('~/code/tangibleai/experiments/mountain-project/boulder-san-diego-county.csv')
ls
cd mountain-project/
ls -hal
df
df['county'].unique()
df['area'].unique()
df.columns
df.head()
df
pd.options.display.max_columns
pd.options.display.max_columns = None
df
df['Rating'].str.contains('V0')
df['V0'] = df['Rating'].str.contains('V0')
df['5_9'] = df['Rating'].str.contains('5.9')
df['V1'] = df['Rating'].str.contains('V1')
df['5_10'] = df['Rating'].str.contains('5.10')
df.sort_values('V0', ascending=False)
df = df.sort_values('V0', ascending=False)
df.head(10)
df.sort_values('Stars')
df.sort_values('Avg Stars')
df.sort_values('Avg Stars', ascending=False)
np = (32.7484460, -117.1301440)
latlon = df['Latitude']
df.columns
latlon = df[['Area Latitude', 'Area Longitude']]
latlon = df[('Area Latitude', 'Area Longitude')]
latlon = df[['Area Latitude', 'Area Longitude']]
latlon
latlon - np.array(np)
me = np
import numpy as np
latlon - np.array(np)
latlon - np.array(me)
md
me
latlon[0]
latlon.iloc[0]
latlon.iloc[2]
latlon.iloc[2] - me
latlon - np.array(me)
hist -f mountain_project.hist.py
